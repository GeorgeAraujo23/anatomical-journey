﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Record : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnGUI()
    {
        GUIStyle label = new GUIStyle(); 
        
        label.fontSize = 40;
        string[] dados = dataBase.loadRecord();
        GUI.Label( new Rect(550, 200, 100, 200), "Player: " + dados[0], label);
        GUI.Label(new Rect(550, 250, 100, 200), "Record: " + dados[1], label);

        GUIStyle g;

        g = GUI.skin.button;
        g.fontSize = 40;

        if (GUI.Button(new Rect(550, 370, 250, 50), "Menu", g))
        {
            SceneManager.LoadScene("Menu");
        }
    }
}
