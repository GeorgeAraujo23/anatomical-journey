﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class gameOver : MonoBehaviour {

    public static int scoreFinal = 0;
    public static bool newRecord = false;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnGUI()
    {
        GUIStyle label = new GUIStyle();
        label = GUI.skin.label;
        label.fontSize = 40;
        GUI.Label(new Rect(400, 50, 250, 50), "Score: " + scoreFinal.ToString());
        if (newRecord) {
            GUI.Label(new Rect(400, 100, 250, 50), "New record!!");
        }
        
        GUIStyle g;

        g = GUI.skin.button;
        g.fontSize = 40;
        if (GUI.Button(new Rect(400, 180, 250, 50), "New Game", g))
        {
            SceneManager.LoadScene("NomePlayer");
        }

        if (GUI.Button(new Rect(400, 250, 250, 50), "Exit", g))
        {
            Application.Quit();
        }

    }


}
