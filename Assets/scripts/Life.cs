﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Life : MonoBehaviour {

    public int hp = 1;
    public bool player;

    public void Damage(int dano)
    {
        hp -= dano;
        if (hp <= 0)
        {
            if (player)
            {
                string[] dados = dataBase.loadRecord();
                gameOver.scoreFinal = controlPlayer.score;
                
                if (!dados[1].Equals(string.Empty))
                {
                    if (float.Parse(dados[1]) < controlPlayer.score)
                    {
                        gameOver.newRecord = true;
                        dataBase.saveRecord(controlPlayer.name, controlPlayer.score.ToString());
                    }
                }
                else {
                    gameOver.newRecord = true;
                    dataBase.saveRecord(controlPlayer.name, controlPlayer.score.ToString());
                }
                
                SceneManager.LoadScene("GameOver");
            }
            else
            {
                Destroy(gameObject, 0);
                IAAsteroids.asteroiCount--;
            }
        }
    }

}
