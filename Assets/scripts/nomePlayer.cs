﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class nomePlayer : MonoBehaviour {

    public string name = "Player";
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnGUI()
    {
        GUIStyle label = new GUIStyle();
        label = GUI.skin.label;
        label.fontSize = 40;
        GUI.Label(new Rect(400, 150, 250, 50), "Name: ", label);
        
        label = GUI.skin.textField;
        label.fontSize = 40;
        name = GUI.TextField(new Rect(400, 200, 250, 50), name, 25, label);
        if (GUI.changed)
        {
            controlPlayer.name = name;
        }
        GUIStyle g;

        g = GUI.skin.button;
        g.fontSize = 40;
        if (GUI.Button(new Rect(400, 300, 250, 50), "Start", g))
        {
            SceneManager.LoadScene("Space");
        }

    }

}
