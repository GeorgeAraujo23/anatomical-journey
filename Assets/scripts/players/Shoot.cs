﻿using UnityEngine;
using System.Collections;

public class Shoot : MonoBehaviour {

    //cria um novo tiro
    public Transform shotPrefab;

    //intervalor entre os tiros
    public float shootintRate = 1.0f;

    //Guarda quanto tempo falta para o proximo tiro
    private float shootCooldown;

    void Start()
    {
        shootCooldown = 0;
    }

    void Update()
    {
        if (shootCooldown > 0)
        {
            shootCooldown -= Time.deltaTime;
            
        }
        if (this.tag == "enemy")
        {
            Attack(this.transform.position, this.transform.rotation.z);
        }
    }

    public void Attack(Vector3 direcao, float angle)
    {

        if (shootCooldown <= 0)
        {
            shootCooldown = shootintRate;
            Transform shootTransform = Instantiate(shotPrefab);
            shootTransform.position = new Vector3(transform.position.x , transform.position.y, transform.position.z);
            shootTransform.Rotate(new Vector3(0, 0, this.transform.localEulerAngles.z - 90));
            
        }

    }
    
}