﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IAAsteroids : MonoBehaviour {

    //cria um novo tiro
    public Transform asteroidPrefab;

    //intervalor entre os tiros
    public static float asteroiintRate = 5.0f;

    public float asteroiMax = 2.0f;

    public static float asteroiCount = 0.0f;

    //Guarda quanto tempo falta para o proximo tiro
    private float asteroiCooldown;

    // Use this for initialization
    void Start () {
        asteroiCooldown = 0;
        controlPlayer cp = this.GetComponent<controlPlayer>();
        asteroiMax = 2 * cp.level;
    }
	
	// Update is called once per frame
	void Update () {
        controlPlayer cp = this.GetComponent<controlPlayer>();
        asteroiMax = 1.5f * cp.level;

        if (asteroiCooldown > 0)
        {
            asteroiCooldown -= Time.deltaTime;
        }

        if (asteroiCooldown <= 0 && asteroiCount <= asteroiMax)
        {
            asteroiCount++;
            asteroiCooldown = asteroiintRate;
            Transform asteroiTransform = Instantiate(asteroidPrefab);
            asteroiTransform.position = new Vector3(Random.Range(-8, 11), 14, 0);

        }
    }
}
