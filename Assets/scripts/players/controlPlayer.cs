﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class controlPlayer : MonoBehaviour {
    public int angle = 1;
    private int angleTotal = 0;

    public bool isMasterTranslate = false;
    public bool isMasterRotate = false;

    public Rigidbody2D rigid2D;
    public float speed = 4;

    public static string name;
    public static int score = 0;
    public int level = 1;

    // Use this for initialization
    void Start()
    {
        rigid2D = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        verificarNivel();
        inputKeyboard();
    }

    void verificarNivel()
    {
        if (score > (20 * level)) {
            level++;
            IAAsteroids.asteroiintRate -= 0.2f;
        }
    }

    void inputKeyboard()
    {
        
        if (isMasterRotate)
        {
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                Shoot shoot = GetComponent<Shoot>();
                if (shoot != null)
                {
                    shoot.Attack(this.transform.position, this.transform.rotation.z);
                }
            }
            if (Input.GetKey(KeyCode.A))
            {
                angleTotal += 1;
                rotate(angle);
            }

            if (Input.GetKey(KeyCode.D))
            {
                angleTotal -= 1;
                rotate(-angle);
            }
        }

        if (isMasterTranslate)
        {
            if (Input.GetKey(KeyCode.W))
            {
                translate(speed);
            }
            else
            {
                translate(0);
            }
        }
    }

    void translate(float speedTranslate)
    {
        this.transform.Translate(new Vector3(speedTranslate, 0, 0));
    }

    void rotate(int angleRotate)
    {
        this.transform.Rotate(0, 0, angleRotate);
    }

    void OnGUI()
    {
        GUIStyle label = new GUIStyle();
        label = GUI.skin.label;
        label.fontSize = 20;
        Life life = this.GetComponent<Life>();
        GUI.Label( new Rect(10, 10, 100, 200) ,"Score: " + score);
        GUI.Label(new Rect(10, 30, 100, 200), "Level: " + level);
        GUI.Label(new Rect(10, 50, 100, 200), "HP: " + life.hp);

    }

}
