using UnityEngine;
using System.Collections;

public class Move : MonoBehaviour {
	public int angle = 1;
    private int angleTotal = 0;

    public bool isMasterTranslate = false;
    public bool isMasterRotate = false;
    
    public Rigidbody2D rigid2D;
    public float speed = 2;

    // Use this for initialization
    void Start () {
        rigid2D = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {

        inputKeyboard();	
	}

    void inputKeyboard()
    {
        if (isMasterRotate)
        {
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                Shoot shoot = GetComponent<Shoot>();
                if (shoot != null)
                {
                    shoot.Attack(this.transform.position, this.transform.rotation.z);
                }
            }
            if (Input.GetKey(KeyCode.A))
            {
                angleTotal += 1;
                rotate(angle);
            }

            if (Input.GetKey(KeyCode.D))
            {
                angleTotal -= 1;
                rotate(-angle);
            }
        }

        if (isMasterTranslate)
        {
            if (Input.GetKey(KeyCode.W))
            {
                translate(speed);
            }
            else
            {
                translate(0);
            }
        }
    }

	void translate(float speedTranslate)
	{
        this.transform.Translate(new Vector3(speedTranslate, 0, 0));
    }

    void rotate(int angleRotate)
    {
        this.transform.Rotate(0, 0, angleRotate);
    }

}
