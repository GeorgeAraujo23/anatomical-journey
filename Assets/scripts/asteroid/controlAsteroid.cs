﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controlAsteroid : MonoBehaviour
{

    public float speed = 0.05f;
    public string objectTag = "Player";
    public int damage = 1;
    // Use this for initialization
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {

       

        if ((this.transform.position.y > 14 && this.transform.localEulerAngles.z != 180) || (this.transform.position.y < -12 && this.transform.localEulerAngles.z != 0))
        {
            this.transform.Rotate(0, 0, 3);
        }

        this.transform.Translate(new Vector3(0, speed, 0));
    }

    

    void OnTriggerEnter2D(Collider2D collider)
    {

        if (collider.gameObject.tag == objectTag)
        {
            Life life = collider.gameObject.GetComponent<Life>();
            life.Damage(damage);
            IAAsteroids.asteroiCount--;
            Destroy(gameObject, 0);
        }

    }
    
}
