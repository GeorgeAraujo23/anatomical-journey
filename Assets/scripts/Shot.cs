﻿using UnityEngine;
using System.Collections;

public class Shot : MonoBehaviour {

    //Velocidade da bala
    public Vector3 speed = new Vector3(1, 0, 0);

    //guarda o movimento da bala
    public Vector2 moviment;

    public int damage = 1;
    public string objectTag;
    public bool isPlayer;

    void Start()
    {
        Destroy(gameObject, 1);
    }

    void OnTriggerEnter2D(Collider2D collider)
    {

        if (collider.gameObject.tag == objectTag || collider.gameObject.tag == "asteroid")
        {

            Life life = collider.gameObject.GetComponent<Life>();
            life.Damage(damage);
            
            Destroy(gameObject, 0);
        }

        if (collider.gameObject.tag != "Player")
        {
            controlPlayer.score += 1;
        }

    }

    // Update is called once per frame
    void Update()
    {
        moviment = new Vector3(speed.x*Time.deltaTime, speed.y * Time.deltaTime, speed.z * Time.deltaTime);
    }

    void FixedUpdate()
    {

        this.transform.Translate(moviment);
    }

    public void rotateShot(float angleRotate)
    {
        this.transform.Rotate(0, 0, angleRotate);
    }

}

