﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour {

    public Texture btnJogar;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnGUI()
    {
        GUIStyle g;
        
        g = GUI.skin.button;
        g.fontSize = 40;
        if (GUI.Button( new Rect((Screen.width/2) - 125, 300, 250, 50), "Play", g))
        {
            SceneManager.LoadScene("NomePlayer");
        }

        if (GUI.Button(new Rect((Screen.width / 2) - 125, 370, 250, 50), "Record", g))
        {
            SceneManager.LoadScene("Record");
        }

        if (GUI.Button(new Rect((Screen.width / 2) - 125, 440, 250, 50), "Exit", g))
        {
            Application.Quit();
        }

    }
}
