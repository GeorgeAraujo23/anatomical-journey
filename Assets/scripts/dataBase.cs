﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dataBase {

    public static void saveRecord(string name, string record) {
        PlayerPrefs.SetString("Name", name);
        PlayerPrefs.SetString("Record", record);
        PlayerPrefs.Save();
    }

    public static string[] loadRecord()
    {
        string name = PlayerPrefs.GetString( "Name");
        string record = PlayerPrefs.GetString("Record");
        string[] result = { name, record };
        return result;
    }

}
